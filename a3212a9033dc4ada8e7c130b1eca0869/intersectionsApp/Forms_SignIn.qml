import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtWebView 1.1
//import Qt.labs.controls 1.0

import ArcGIS.AppFramework 1.0
import ArcGIS.AppFramework.Controls 1.0
import ArcGIS.AppFramework.Runtime 1.0
import ArcGIS.AppFramework.Runtime.Controls 1.0

ColumnLayout {
    id: signInForm
    anchors.fill: parent
    enabled: true

    property var signInFields : []
    spacing: 20

    property double scaleFactor: AppFramework.displayScaleFactor
    property Settings settings


    signal buttonClicked

    Image{
        source:"../images/safecoal_2.png"
        Layout.alignment: Qt.AlignHCenter
    }

    Item {
        Layout.fillHeight: true
    }
    Section_Header{
        headerText: "Team Leader"
    }
    TextField{
        id: txtUserName
        Layout.fillWidth: true
        font{
            pointSize: 15
        }
    }

    Section_Header {
        headerText: "Team"
    }
    TextField{
        id: txtTeam
        Layout.fillWidth: true
        font{
            pointSize: 15
        }

    }
    RowLayout {
        Layout.fillWidth: true
        BusyIndicator {
            Layout.alignment: Qt.AlignLeft
            running: !portalComponents.hasLicense
            visible: running
            enabled: running

            onRunningChanged: running ? appContent.statusText.text = "Activating license on ArcGIS.com..." : ""
        }

        Button {
            text: "SUBMIT"
            enabled: txtUserName.text> "" && txtTeam.text > ""
            onClicked: {
                app.jsonRelatedInspection.attributes.Leader = txtUserName.text //portalComponents.portal.user.username;
                app.jsonRelatedInspection.attributes.Team = txtTeam.text //portalComponents.portal.user.username;

                signInForm.buttonClicked();
            }
        }
    }

    Item {
        Layout.fillHeight: true
    }

//    Connections {
//        target: ArcGISRuntime.identityManager

//                onOAuthCodeRequired: {
//                    webView.authUrl = authorizationUrl;
//                    webView.url = authorizationUrl;
//                    webView.visible = true;
//                }
//    }

    //    Item {
    //        Column {
    //            anchors {
    //                fill: parent
    //                margins: 10 * scaleFactor
    //            }
    //            spacing: 10 * scaleFactor

    //            Image {
    //                width: 50 * scaleFactor
    //                height: 50 * scaleFactor
    //                fillMode: Image.PreserveAspectFit
    //                source: portalComponents.portal.signedIn ? portalComponents.portal.user.thumbnailUrl : ""
    //                visible : {
    //                    if (portalComponents.portal.signedIn) {


    //                        if (portalComponents.portal.user.thumbnailUrl)
    //                            visible = true;
    //                    } else {
    //                        visible = false;
    //                    }
    //                }
    //            }

    //            Text {
    //                width: app.width
    //                font.pixelSize: 14 * scaleFactor
    //                wrapMode: Text.WordWrap
    //                text: portalComponents.portal.signedIn ? "Full name: " + portalComponents.portal.user.fullName : ""
    //            }

    //            Text {
    //                width: app.width
    //                font.pixelSize: 14 * scaleFactor
    //                wrapMode: Text.WordWrap
    //                text: portalComponents.portal.signedIn ? "Created on: " + portalComponents.portal.user.created : ""
    //            }

    //            Text {
    //                width: app.width
    //                font.pixelSize: 14 * scaleFactor
    //                wrapMode: Text.WordWrap
    //                text: portalComponents.portal.signedIn ? "Modified on: " + portalComponents.portal.user.modified: ""
    //            }

    //            Text {
    //                width: app.width
    //                font.pixelSize: 14 * scaleFactor
    //                wrapMode: Text.WordWrap
    //                text: portalComponents.portal.signedIn ? "Organization Id: " + portalComponents.portal.portalInfo.organizationId : ""
    //            }

    //            Text {
    //                width: app.width - app.width/10
    //                wrapMode: Text.WrapAnywhere
    //                font.pixelSize: 14 * scaleFactor
    //                text: portalComponents.portal.signedIn ?  "License string: " + portalComponents.portal.portalInfo.licenseInfo.json["licenseString"] : ""
    //            }
    //        }
    //    }

//        WebView {
//            id: webView
//            property string authUrl

//            Layout.fillHeight: true
//            Layout.fillWidth: true
//            visible: false

//            onLoadingChanged: {
//                console.log("from settings.json:", app.settings.value("token"));
//                console.log(title.indexOf("SUCCESS code="))
//                if (title.indexOf("SUCCESS code=") > -1) {
//                    var authCode = title.replace("SUCCESS code=", "");
//                    ArcGISRuntime.identityManager.setOAuthCodeForUrl(authUrl, authCode);
//                    webView.visible = false;
//                    appContent.customState = "";
//                }
//            }
//        }
}
