import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

import ArcGIS.AppFramework.Runtime 1.0

Item {
    property var furtherActionsArray: []

    ColumnLayout {
        anchors{
            left: parent.left
            right: parent.right
            top: parent.top
            leftMargin: 5
            rightMargin: 5
            topMargin: 5
        }

        clip: true
        spacing: 10


        Section_Header {
            headerText: "Further Comments"
            Layout.alignment: Qt.AlignTop
            Layout.fillWidth: true

        }

        RowLayout {
            Layout.fillWidth:true
            TextField {
                id: txtElement
                Layout.preferredWidth: 30
                text: "4"
                readOnly: true
                font{
                    pointSize: 14
                }
            }
            TextField {
                id: txtAction
                Layout.fillWidth:true
                placeholderText:  "Action Detail"
                font{
                    pointSize: 14
                }
                onTextChanged: jsonFurtherAction.attributes.ActionDetail = text
            }
            TextField {
                id: txtWho
                Layout.fillWidth:true
                placeholderText:  "By who"
                font{
                    pointSize: 14
                }
                onTextChanged: jsonFurtherAction.attributes.ByWho = text

            }
            Rectangle {
                id: dateButton
                Layout.preferredWidth: buttonText.paintedWidth * 1.1
                Layout.preferredHeight: buttonText.paintedHeight * 1.15
                color:  app.info.propertyValue("default-primary-color")

                Text {
                    id: buttonText
                    text: "By when"
                    font{
                        pointSize: 14
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: calendar.visible = !calendar.visible
                }
            }
        }
        RowLayout {
            Layout.fillWidth: true
            spacing: 5
            Button {
                text: "Submit"

                onClicked: {
                    jsonFurtherAction.attributes.CreationDate = Date.now()
                    jsonFurtherAction.attributes.EditDate = Date.now();
                    jsonFurtherAction.attributes.ElementNumber = 4;
                    jsonFurtherAction.attributes.Status = "A";

                    if (!appContent.isNewFeature){

                    }

                    console.log(JSON.stringify(jsonFurtherAction, undefined, 2));

                    furtherActionsArray.push(jsonFurtherAction);

                    repeater.model = furtherActionsArray;

                    //Clean up
                    jsonFurtherAction = {"attributes":{}};
                    txtAction.text = "";
                    txtWho.text = "";
                    txtElement.text = 4;
                }
            }
            Item {
                Layout.fillWidth: true
            }

            Button {
                text: "Cancel"
            }
        }
        Item {
            Layout.preferredHeight: 50
            Layout.fillWidth: true
        }

        Repeater {
            id: repeater
            Layout.fillWidth: true
            Layout.fillHeight: true
            model: furtherActionsArray

            delegate: RowLayout {
                Layout.fillWidth:true
                TextField {
                    Layout.preferredWidth: 30
                    text: "4"
                    readOnly: true
                    enabled: false
                    font{
                        pointSize: 14
                    }
                }
                TextField {
                    Layout.fillWidth:true
                    text: modelData.attributes.ActionDetail //modelData.attributes.ActionDetail //"Action Detail"
                    enabled: false
                    font{
                        pointSize: 14
                    }
                }
                TextField {
                    Layout.fillWidth:true
                    text: modelData.attributes.ByWho //"By who"
                    enabled: false
                    font{
                        pointSize: 14
                    }
                }
                TextField {
                    Layout.fillWidth:true
//                    text: Date.now(); //new Date(modelData.attributes.byWhen).toLocaleDateString()

                    text: {
                        var d = new Date(modelData.attributes.ByWhen);
                        return d.toLocaleDateString(Qt.LocalDate);
                   }

                    enabled: false
                    font{
                        pointSize: 14
                    }
                }
            }
        }

        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }

    Calendar {
        id: calendar
        visible: false
        anchors.fill: parent

        property var locale: Qt.locale()

        onClicked: {
            visible = !visible
            console.log(selectedDate );
            var d = new Date(selectedDate);
            console.log(d.getTime());
            jsonFurtherAction.attributes.ByWhen = d.getTime();

            buttonText.text = d.toLocaleDateString(Qt.LocalDate);

            console.log(d.toDateString());
            console.log(d.toLocaleDateString());
            console.log(d.toLocaleDateString(Qt.LocalDate));
        }
    }
}



