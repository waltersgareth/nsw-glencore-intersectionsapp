import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

import ArcGIS.AppFramework.Runtime 1.0

Item {
    clip: true
    property string pageToPush: "map"
    property bool isFormCamera: false
    property bool isFileDialog: false

    onPageToPushChanged: {
        console.log ("isFormCamera", isFormCamera);
        if (pageToPush == "photos"){
            loader.setSource("Camera_Page.qml",{"isLoadedFromForm": isFormCamera, "fdVisible": isFileDialog })
        }
        else loader.source = "";
    }
    property alias furtherAction: furtherAction
    property alias map: map

    GlencoreMap{
        id: map
        anchors.fill: parent
        visible: pageToPush == "map"
    }

    Loader {
        id: loader
        anchors.fill: parent
        visible: status == Loader.Ready && pageToPush == "photos"
    }


    Forms_FurtherAction {
        id: furtherAction
        anchors.fill: parent
        visible: pageToPush == "actions"
    }
}
