import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtMultimedia 5.5
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.0

import ArcGIS.AppFramework 1.0
import ArcGIS.AppFramework.Controls 1.0

Rectangle {
    color: app.info.propertyValue("accent-color")

    property var filesArray: []
    property bool isLoadedFromForm: false
    property bool fdVisible: false

    Photos_FileFolder {
        id: fileFolder
        onPathChanged: {
            filesArray = fileFolder.fileNames();
            //fileDialog.folder = fileFolder.path + "/justCamera";
            fileDialog.visible = fdVisible;
        }
    }

    Timer {
        id: timer
        interval: 2000
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        //folder: AppFramework.standardPaths.standardLocations(StandardPaths.PicturesLocation)[0] + "/IntersectionsApp/justCamera"
        folder: shortcuts.pictures + "/IntersectionsApp/justCamera"
        selectMultiple: false
        nameFilters: [ "Image files (*.jpg)" ]
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls);
            var d = new Date();
            var formattedPhotoName = app.photoPrefix + "_" + d.getHours() + d.getMinutes() + d.getSeconds() + ".jpg"
            var result = fileFolder.renameFile(AppFramework.resolvedPath(fileUrl), fileFolder.path + "/" + formattedPhotoName);
            console.log("rename result", result);
            filesArray = fileFolder.fileNames();
        }
        onRejected: {
            console.log("Canceled")
        }
    }

    ColumnLayout {
        anchors.fill: parent
        spacing:2
        Rectangle {
            id: cameraRect
            color: app.info.propertyValue("accent-color")
            Layout.fillWidth: true
            Layout.fillHeight: true

            Camera {
                id: camera
                captureMode: Camera.CaptureStillImage

                imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash

                exposure {
                    exposureCompensation: -1.0
                    exposureMode: Camera.ExposureAuto
                }

                flash.mode: Camera.FlashRedEyeReduction

                imageCapture.onImageSaved: {
                    if (isLoadedFromForm) {
                        var tfilesArray = fileFolder.fileNames("*.jpg");
                        console.log(tfilesArray.length, filesArray.length)
                        var d = new Date();
                        var formattedPhotoName = app.photoPrefix + "_" + d.getHours() + d.getMinutes() + d.getSeconds() + ".jpg"
                        fileFolder.renameFile(tfilesArray[tfilesArray.length-1], formattedPhotoName);
                        filesArray = fileFolder.fileNames()
                    }
                    else {
                        timer.running = true;
                    }

                }
            }

            VideoOutput {
                source: camera
                anchors.fill: parent
                focus : visible // to receive focus and capture key events when visible
            }

            Rectangle {
                anchors{
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                    rightMargin: 5
                }
                color: app.info.propertyValue("secondary-text-color")
                opacity: 0.8
                width: app.info.propertyValue("primary-button-size")
                height: width
                radius: width * 0.5
                Image {
                    source: "../icons/ic_photo_camera_Orange_36pt_2x.png"
                    anchors.centerIn: parent
                    width: parent.width * 0.75
                    height: width
                    //opacity: 1
                }

                visible: camera.imageCapture.ready

                MouseArea {
                    anchors.fill: parent
                    onClicked:{
                        if (isLoadedFromForm){
                            camera.imageCapture.captureToLocation( fileFolder.path );
                        }
                        else{
                            camera.imageCapture.captureToLocation( fileFolder.path + "/justCamera" );
                        }

                    }
                }
            }

            Rectangle {
                id: cameraText

                anchors{
                    centerIn: parent
                }
                color: app.info.propertyValue("secondary-text-color")
                opacity: 0.8
                width: ctxt.paintedWidth * 1.2
                height: ctxt.paintedHeight * 1.2
                visible: timer.running
                radius: 5

                Text {
                    id: ctxt
                    text: "Photo captured"
                    anchors.centerIn: parent
                    color: app.info.propertyValue("default-primary-color")
                    //visible: timer.running

                    font{
                        pointSize: 15
                    }
                }
            }
        }

        Rectangle{
            Layout.preferredHeight: 230
            Layout.maximumHeight: 230
            Layout.fillWidth: true
            color: app.info.propertyValue("divider-color")
            visible: isLoadedFromForm


            Rectangle{
                id: folderButton
                height: 75
                width: height
                anchors{
                    left: parent.left
                    leftMargin: 10
                    verticalCenter: parent.verticalCenter
                }
                color: ma.pressed ? app.info.propertyValue("accent-color") : app.info.propertyValue("default-primary-color")
                radius: height * 0.5

                layer.enabled: true
                layer.effect: Glow {
                    samples: 10
                    color: app.info.propertyValue("divider-color")
                    transparentBorder: true
                }

                Image{
                    source: "../icons/ic_folder_open_white_36dp.png"

                    anchors.centerIn: parent
                    width: parent.width * 0.75
                    height: width
                }

                MouseArea {
                    id: ma
                    anchors.fill: parent
                    onClicked: {
                        fileDialog.visible = true
                    }
                }
            }

            Flickable {
                enabled: width < contentWidth
                anchors {
                    left: folderButton.right
                    top: parent.top
                    bottom: parent.bottom
                    right: parent.right
                    margins: 10
                }
                flickableDirection: Flickable.HorizontalFlick
                clip: true
                contentHeight: photosRow.height
                contentWidth: photosRow.width

                RowLayout {
                    id: photosRow

                    spacing: 10

                    Repeater{
                        id: repeater
                        anchors.fill: parent
                        anchors.margins: 5

                        model: filesArray.reverse()

                        Rectangle {
                            width: 300
                            height: 169
                            color: app.info.propertyValue("default-primary-color")
                            Image {
                                anchors.fill: parent
                                fillMode: Image.PreserveAspectFit
                                source: AppFramework.resolvedPathUrl(fileFolder.file(modelData).path)
                            }
                        }
                    }
                }
            }
        }
    }
}
