import QtQuick 2.0

Rectangle {
    color: app.info.propertyValue("secondary-text-color")
    opacity: 0.95
    visible: false
    z:100

    MouseArea{
        anchors.fill: parent
        onClicked: parent.visible = false
    }
}


