import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtMultimedia 5.5

import ArcGIS.AppFramework 1.0
import ArcGIS.AppFramework.Controls 1.0

FileFolder {
    id: fileFolder
    path:  AppFramework.standardPaths.standardLocations(StandardPaths.PicturesLocation)[0]

    Component.onCompleted: {
        var mainPath = path;
        var mainFolder = "IntersectionsApp";
        var newFileFolder = fileFolder.folder(mainFolder);
        var folderResult = newFileFolder.makeFolder();
        if(folderResult){
            fileFolder.path = mainPath + "/" + mainFolder;

            var jcFolder = fileFolder.folder("justCamera");

            var jcFolderResult = jcFolder.makeFolder();

            if(!jcFolderResult)
                appContent.statusText.text = "Couldn't create either of the sub folders for the photos"
        }
        else {
           fileFolder.path = mainPath;
           appContent.statusText = "Couldn't create the Intersections app photo folder."
        }
        console.log("the main photo folder is:", fileFolder.path);
    }
}
