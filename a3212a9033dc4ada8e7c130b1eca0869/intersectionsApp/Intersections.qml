/* Copyright 2015 Esri
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import QtPositioning 5.3

import ArcGIS.AppFramework 1.0
import ArcGIS.AppFramework.Controls 1.0
import ArcGIS.AppFramework.Runtime 1.0
import ArcGIS.AppFramework.Runtime.Controls 1.0

//------------------------------------------------------------------------------

App {
    id: app
    width: 900
    height: 700

    Rectangle{
        id: appRect
        anchors.fill: parent
        color: app.info.propertyValue("divider-color")
    }

    property string gdbPath: AppFramework.standardPaths.standardLocations(StandardPaths.AppDataLocation)[0] + "/Geodatabase"
    property string gdbName: "GlendellIntersections.geodatabase"

    property var appInfoFields : app.info.propertyValue("fields")
    property var editableFields: []

    property var jsonFeature: {"attributes":{}, "geometry":{}}
    property var jsonRelatedInspection: {"attributes":{}}
    property var appFurtherActionsArray: []
    property var jsonFurtherAction: {"attributes":{}}
    property var jsonInspectionAttachments: ({})

    onJsonRelatedInspectionChanged: console.log("changed:", JSON.stringify(jsonRelatedInspection, undefined, 2))

    property var mapExtent
    property bool isServiceInfoOK: false

    property string photoPrefix : ""

    //    property alias portalComponents: portalComponents

    property date activatedTimestamp


    PositionSource {
        id: positionSource
        updateInterval: 3000

        Component.onCompleted: {
            console.log("positionSource:", name);
        }

        onActiveChanged: {
            console.log("positionSource.active:", active);
        }

        onPositionChanged: {
            var position = positionSource.position;

            if (position.latitudeValid && position.longitudeValid && position.timestamp >= activatedTimestamp) {
                console.log("newPosition coordinate:", position.timestamp, position.coordinate.latitude, position.coordinate.longitude, position.coordinate.altitude);
            }
        }

        onSourceErrorChanged: {
            if (positionSource.sourceError !== PositionSource.NoError) {
                console.error("Deactivating positioning sourceError:", positionSource.sourceError);

                switch (positionSource.sourceError) {
                case PositionSource.AccessError :
                    appContent.statusText.text = qsTr("Position source access error");
                    break;

                case PositionSource.ClosedError :
                    appContent.statusText.text = qsTr("Position source closed error");
                    break;

                case PositionSource.SocketError :
                    appContent.statusText.text = qsTr("Position source error");
                    break;

                case PositionSource.NoError :
                    appContent.statusText.text = "";
                    break;

                default:
                    appContent.statusText.text = qsTr("Unknown position source error %1").arg(positionSource.sourceError);
                    break;
                }
            }
        }
    }

    ServiceInfoTask {
        id: serviceInfoTask
        property string fsURL: app.info.propertyValue("featureServiceUrl");
        url:  fsURL

        onFeatureServiceInfoStatusChanged: if (featureServiceInfoStatus == Enums.FeatureServiceInfoStatusCompleted){
                                               appContent.statusText.text =  "service info completed. GDB exists? : " + gdbComponents.gdbFileInfo.exists + "."  ;

                                               isServiceInfoOK = true;

                                               if (!gdbComponents.gdbFileInfo.exists){
                                                   appContent.statusText.text = "initialised.."
                                                   console.log ("before:", JSON.stringify(gdbComponents.generateGeodatabaseParameters.json, undefined, 2));

                                                   gdbComponents.generateGeodatabaseParameters.extent =  mapExtent; //mainMap.extent;
                                                   //gdbComponents.generateGeodatabaseParameters.outSpatialReference =
                                                   appContent.statusText.text = "extent set..."
                                                   gdbComponents.generateGeodatabaseParameters.layerIds = [0,1,2,3];
                                                   appContent.statusText.text = "layerids set..."
                                                   gdbComponents.generateGeodatabaseParameters.returnAttachments = true;
                                                   appContent.statusText.text = "attachments set..."
                                                   gdbComponents.generateGeodatabaseParameters.syncModel = Enums.SyncModelLayer

                                                   console.log ("after:", JSON.stringify(gdbComponents.generateGeodatabaseParameters.json, undefined, 2));

                                                   appContent.statusText.text = "Starting generate geodatabase task...w";
                                                   gdbComponents.geodatabaseSyncTask.generateGeodatabase(gdbComponents.generateGeodatabaseParameters, gdbPath + "/" + gdbName);
                                               }
                                               else {
                                                   //                                                   statusText.text = "Will need to do a sync here"
                                                   appContent.statusText.text = ""
                                               }

                                           }
                                           else if (featureServiceInfoStatus == Enums.FeatureServiceInfoStatusErrored){
                                               appContent.statusText.text =  "There was error accessing the service. Contact your administrator.";
                                               isServiceInfoOK = false;
                                           }
                                           else if (featureServiceInfoStatus == Enums.FeatureServiceInfoStatusInProgress){
                                               appContent.statusText.text = "Retreiving service info: In progress";
                                           }
    }

    App_Content {
        id: appContent

        anchors.fill: parent
        spacing: 2
    }

    PopUp_Instructions{
        id: popInstructions
        anchors.fill: parent
        z: 100
    }

    GeodatabaseComponents{
        id: gdbComponents
    }

    AuthenticationComponents {
        id: portalComponents
    }

    Component.onCompleted:  {
        if (AppFramework.settings.value("license/licenseInfoJson")){
            console.log("reading local license...")
            console.log(JSON.stringify(AppFramework.settings.value("license/licenseInfoJson")))
            appContent.statusText.text = "You are already signed in, just add the team leader details"
            portalComponents.setLicense()
        }
        else {
            console.log("this is weird...")
            portalComponents.portal.signIn();
        }
    }
}

//------------------------------------------------------------------------------
