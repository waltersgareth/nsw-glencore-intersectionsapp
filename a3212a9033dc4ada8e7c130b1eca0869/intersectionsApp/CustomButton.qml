import QtQuick 2.3
//import QtQuick.Controls 1.2
import Qt.labs.controls 1.0
import QtGraphicalEffects 1.0

import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4
import QtPositioning 5.3

//------------------------------------------------------------------------------

Rectangle{
    Layout.fillHeight: true
    Layout.preferredWidth: height
    color: enabled ? ma.pressed ? app.info.propertyValue("accent-color") : app.info.propertyValue("default-primary-color") : app.info.propertyValue("accent-color")
    radius: Layout.maximumHeight * 0.5

    property string imageName: ""

    layer.enabled: true
    layer.effect: Glow{
        samples: 10
        color: app.info.propertyValue("divider-color")
        transparentBorder: true
    }

    Image{
        source: "../icons/" + imageName

        anchors.centerIn: parent
        width: parent.width * 0.75
        height: width
    }
}

