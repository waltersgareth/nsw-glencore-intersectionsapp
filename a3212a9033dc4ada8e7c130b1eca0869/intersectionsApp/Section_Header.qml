import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Rectangle {
    id: sectionRect
    color: app.info.propertyValue("default-primary-color")
    Layout.fillWidth: true
    Layout.preferredHeight: txtHeader.paintedHeight * 1.2

    property alias headerText: txtHeader.text

    Text{
        anchors{
            verticalCenter: parent.verticalCenter
            left: parent.left
            leftMargin: 5
        }

        id: txtHeader
        text: headerText
        font{
            pointSize: 14
        }
        color: app.info.propertyValue("text-primary-color")
    }

    Rectangle{
        width: parent.width
        height: 2
        anchors.bottom: parent.bottom

        color: app.info.propertyValue("accent-color")
    }
}
