import QtQuick 2.3
import QtPositioning 5.3

import ArcGIS.AppFramework 1.0
import ArcGIS.AppFramework.Controls 1.0
import ArcGIS.AppFramework.Runtime 1.0
import ArcGIS.AppFramework.Runtime.Controls 1.0

Map {
    id: map
    wrapAroundEnabled: true
    rotationByPinchingEnabled: true
    magnifierOnPressAndHoldEnabled: true
    mapPanningByMagnifierEnabled: true
    zoomByPinchingEnabled: true

    property var selectedFeatureId: null

    property alias mCenter : extent.center
    property alias offlineLayer: offlineLayer

    positionDisplay {
        positionSource: positionSource
    }

    //This sets the initial extent of the map. This was based on the service initial extent
    extent: Envelope {
        id: initialExtent
        xMin: 316887
        yMin: 6407096.438
        xMax: 322034
        yMax: 6414480

    }

    ArcGISDynamicMapServiceLayer {
        url: app.info.propertyValue("basemapServiceUrl", "http://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer")
    }

    FeatureLayer {
        id: offLinePits
        featureTable: gdbComponents.gdbFeatureTablePoly
    }

    FeatureLayer {
        id: offlineLayer
        featureTable: gdbComponents.gdbFeatureTable

        selectionColor: "cyan"
        enableLabels: true

        function hitTestFeatures(x,y) {
            offlineLayer.clearSelection();

            var tolerance = Qt.platform.os === "ios" || Qt.platform.os === "android" ? 4 : 1;
            var featureIds = offlineLayer.findFeatures(x, y, tolerance * AppFramework.displayScaleFactor, 1);
            if (featureIds.length > 0) {
                selectedFeatureId = featureIds[0];
                selectFeature(selectedFeatureId);
                var featureToEdit = offlineLayer.featureTable.feature(selectedFeatureId);
                statusText.text = JSON.stringify(featureToEdit.attributes, undefined, 2);
                statusText.text = "selectedFeatureId: " + selectedFeatureId + ", IntID:" + featureToEdit.attributes.IntersectionID;

                appContent.customState = "add new";
                appContent.isNewFeature = false;
                appContent.forms_Section.existingInspectionId = featureToEdit.attributes.IntersectionID;
                appContent.forms_Section.existnigInspectionObjectId = featureToEdit.attributes.OBJECTID;
                appContent.forms_Section.existingFeature = featureToEdit;

                console.log("about to query exising:", appContent.forms_Section.existingInspectionId )
                gdbComponents.inspectionQuery.where = "IntersectionID = '" +  featureToEdit.attributes.IntersectionID + "'";
                gdbComponents.inspectionQuery.returnGeometry = false;
                gdbComponents.inspectionQuery.outFields = "*";
                gdbComponents.inspectionQuery.maxFeatures = 5;
                gdbComponents.gdbInspectionsFeatureTable.queryFeatures(gdbComponents.inspectionQuery);
            }
        }
    }

    CustomZoomButtons{
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
            margins: 10
        }
    }

    Envelope {
        id: extent
        xMin: map.extent.xMin
        yMin: map.extent.yMin
        xMax: map.extent.xMax
        yMax: map.extent.yMax

        onCenterChanged: {
            if (centerX){
                console.log("Envelope (Extent) center", centerX, centerY);
                app.mapExtent = extent;
            }
        }
    }

    Image {
        anchors{
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -(height * 0.5)
        }

        source: "../icons/ic_location_on_Orange_48dp.png"
    }

    onStatusChanged: {
        if (status == Enums.MapStatusReady) {
            console.log("Map Ready!!");
            app.mapExtent = extent;
            positionSource.start();
        }
    }

    onMouseClicked: {
        if (!appContent.isNewFeature)
            offlineLayer.hitTestFeatures(mouse.x, mouse.y);
    }

    Connections {
        target: appContent

        onIsNewFeatureChanged:{
            if(appContent.isNewFeature){
                offlineLayer.clearSelection();
            }
        }
    }

    Connections {
        target: positionSource
        onPositionChanged: {
            var position = positionSource.position;
            if (position.latitudeValid && position.longitudeValid && position.timestamp >= activatedTimestamp) {
                console.log("newPosition coordinate:", position.timestamp, position.coordinate.latitude, position.coordinate.longitude, position.coordinate.altitude);
                jsonFeature.geometry.x = centerX;
                jsonFeature.geometry.y = centerY;
            }
        }


    }

}

