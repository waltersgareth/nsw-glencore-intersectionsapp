import QtQuick 2.0
import QtQuick.Layouts 1.1

PopUp_Base {
    ColumnLayout {
        anchors {
            fill: parent
            margins: 10
        }

        spacing: 5
        RowLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            spacing: 5
            ColumnLayout {
                Layout.fillHeight: true
                Layout.fillWidth: true
                Section_Header{
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    headerText: "T Intersection"
                }
                Image {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    source: "../images/T_Intersection.PNG"
                    fillMode: Image.PreserveAspectFit
                }
            }
            ColumnLayout {
                Layout.fillHeight: true
                Layout.fillWidth: true
                Section_Header{
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    headerText: "Four Way Intersection"
                }
                Image {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    source: "../images/4Way_Intersection.PNG"
                    fillMode: Image.PreserveAspectFit
                }
            }
        }

        Section_Header {
            Layout.fillHeight: true
            Layout.fillWidth: true
            headerText: "Y Intersections"
        }
        Image {
            Layout.fillHeight: true
            Layout.fillWidth: true
            source: "../images/Y_Intersections.PNG"
            fillMode: Image.PreserveAspectFit
        }
    }
}


