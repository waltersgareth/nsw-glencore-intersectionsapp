import QtQuick 2.3

import ArcGIS.AppFramework 1.0
import ArcGIS.AppFramework.Controls 1.0
import ArcGIS.AppFramework.Runtime 1.0
import ArcGIS.AppFramework.Runtime.Controls 1.0

//------------------------------------------------------------------------------

Item {
    property alias portal: portal
    property alias userCredentials: userCredentials
    property bool isOnline: AppFramework.network.isOnline

    property bool hasLicense: false

    property alias isSignedIn: portal.signedIn

    onIsOnlineChanged: {
        if (isOnline && !isSignedIn){
            "this might be the problem.."
            portal.signIn()
        }
        else if (!isOnline) {
                appContent.statusText.text = "No network connection..."
            }

    }


    signal arcGISRuntimeLicenseExpiryChanged

//    onArcGISRuntimeLicenseExpiryChanged: {
//        console.log("from custom signal expiry:", ArcGISRuntime.license.expiry);
//        app.settings.setValue("licenseExpiry", ArcGISRuntime.license.expiry);
//        console.log()
//    }

    UserCredentials{
        id: userCredentials
        userName: "xstrata"
        password: "portal123"

//        oAuthClientInfo: OAuthClientInfo {
//            clientId: app.info.value("deployment").clientId
//            oAuthMode: Enums.OAuthModeUser
//        }
    }
    
    Portal {
        id: portal
        credentials: userCredentials

        url: "https://ausydsrv4c613.anyaccess.net/portalarcgis/"

        onSignInError: {
            if (!AppFramework.network.isOnline){
                appContent.statusText.text = "Can't connect to network to authenticate"
            }
        }

        onSignInComplete: {
            if (signedIn){
                // Store License Info in a settings file
                AppFramework.settings.setValue("license/licenseInfoJson",  portal.portalInfo.licenseInfo.json);
                appContent.statusText.text = "Level: " + ArcGISRuntime.license.licenseLevel

                ArcGISRuntime.license.setLicense(licenseInfo);

                console.log("signal expiry:", ArcGISRuntime.license.expiry);
                var d = new Date();
                var ed = new Date(ArcGISRuntime.license.expiry);
                var daysRemaining = Math.floor((ed-d) / (24*60*60*1000))
                appContent.statusText.text = "Lic Level? " + ArcGISRuntime.license.licenseLevel +", Days remaining: " + daysRemaining

                if(AppFramework.network.isOnline){
                    serviceInfoTask.fetchFeatureServiceInfo();
                }

                hasLicense = true
            }
            else{
                console.log("not signed in")
            }
        }
    }

    LicenseInfo {
        id: licenseInfo
    }

    function setLicense () {
         appContent.statusText.text = "getting stored license..."
        var licenseJson = AppFramework.settings.value("license/licenseInfoJson");
        console.log("licenseJson", JSON.stringify(licenseJson));

        licenseInfo.json = licenseJson ;

        ArcGISRuntime.license.setLicense(licenseInfo);
        console.log("From set license:", ArcGISRuntime.license.expiry);
        console.log("From set license:", "Level: " + ArcGISRuntime.license.licenseLevel)

        appContent.statusText.text = "Lic Level? " + ArcGISRuntime.license.licenseLevel

        hasLicense = true;
    }
}
