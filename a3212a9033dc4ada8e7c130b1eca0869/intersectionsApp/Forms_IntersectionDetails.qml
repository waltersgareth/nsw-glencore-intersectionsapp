import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

import ArcGIS.AppFramework 1.0
import ArcGIS.AppFramework.Runtime 1.0
import ArcGIS.AppFramework.Runtime.Controls 1.0

Item {
    id: itemp

    property int buttonPressed : 1

    property var intersectionFields: app.info.propertyValue("fields")
    property var requiredFields: []
    property var considerationsArray: {
        var ta = [];
        for (var f in intersectionFields){
            if (intersectionFields[f].section === "considerations" && intersectionFields[f].required === true)
                ta.push(intersectionFields[f])
        }
        requiredFields = ta;
        return ta;
    }

    property var optionalFields: []
    property var considerationsOptionalArray: {
        var oa = [];
        for (var i in intersectionFields){
            if (intersectionFields[i].section === "considerations" && intersectionFields[i].required === false)
                oa.push(intersectionFields[i])
        }
        optionalFields = oa;
        return oa;
    }

    property var previousInpsections: []

    property alias fileIndex: submitButton.addImagesCount

    property string pitId: ""

    property var intersectionIds: []

    signal gdbFeatureTablePolyResult

    onGdbFeatureTablePolyResult: {
        if (appContent.isNewFeature){
            if ( gdbComponents.gdbFeatureTablePoly.queryFeaturesStatus === Enums.QueryFeaturesStatusCompleted){
                var featureCount = gdbComponents.gdbFeatureTablePoly.queryFeaturesResult.featureCount;

                if (featureCount == 0){
                    pitId = "CO";
                }
                else {
                    var iterator = gdbComponents.gdbFeatureTablePoly.queryFeaturesResult.iterator;

                    if(!iterator.hasNext()){
                        return;
                    }
                    while(iterator.hasNext()){
                        var feature = iterator.next();
                        console.log("feature: ", feature.attributeValue("PitID"));
                        pitId = feature.attributeValue("PitID");
                    }
                }

                //Query to find the Max ID from the selected region
                var whereString = "IntersectionID like '" + pitId + "%'";
                gdbComponents.idQuery.where = whereString;
                gdbComponents.idQuery.returnGeometry = false;
                gdbComponents.idQuery.outFields = "IntersectionID";

                gdbComponents.gdbFeatureTable.queryFeatures(gdbComponents.idQuery);
            }
        }
    }

    signal gdbFeatureResult

    signal gdbFeatureTableInspectionsResult

    onGdbFeatureTableInspectionsResult: {
        console.log("list existing inspections")
        console.log(gdbComponents.gdbInspectionsFeatureTable.queryFeaturesResult.featureCount);

        var featureCount = gdbComponents.gdbInspectionsFeatureTable.queryFeaturesResult.featureCount;
        if (featureCount > 0 ){
            console.log( gdbComponents.gdbInspectionsFeatureTable.queryFeaturesResult)
            var iterator = gdbComponents.gdbInspectionsFeatureTable.queryFeaturesResult.iterator;
            if(!iterator.hasNext())
                return;
            else {
                console.log("feature has next")
            }

            while(iterator.hasNext()){
                var feature = iterator.next();
                console.log(feature.attributeValue("GlobalID"),
                            feature.attributeValue("Sign_Type"),
                            feature.attributeValue("created_date"),
                            feature.attributeValue("Leader"),
                            feature.attributeValue("Team")
                            )
                previousInpsections.push([
                                             feature.attributeValue("Sign_Type"),
                                             feature.attributeValue("created_date"),
                                             feature.attributeValue("Leader"),
                                             feature.attributeValue("Team")
                                         ])
            }
            prevInspections.model = previousInpsections.reverse()
            prevHeader.headerText += (" " + featureCount);
        }
    }

    function splitId(idToSplit){
        console.log(idToSplit.substring(2));

        //create the new id
        var formattedNumber = Number(idToSplit.substring(2)) + 1000001;
        formattedNumber = formattedNumber.toString().substring(1);
        idText.text = pitId + (formattedNumber);
    }

    onGdbFeatureResult: {
        if ( gdbComponents.gdbFeatureTable.queryFeaturesStatus === Enums.QueryFeaturesStatusCompleted){
            var featureCount = gdbComponents.gdbFeatureTable.queryFeaturesResult.featureCount;
            var formattedNumber;
            console.log("Feature Table result:", featureCount);

            if (featureCount > 0){
                var iterator = gdbComponents.gdbFeatureTable.queryFeaturesResult.iterator;

                if(!iterator.hasNext())
                    return;

                while(iterator.hasNext()){
                    var feature = iterator.next();
                    console.log("feature: ", feature.attributeValue("IntersectionID"));
                    intersectionIds.push ( feature.attributeValue("IntersectionID").toUpperCase() );
                }

                intersectionIds.sort();
                intersectionIds.reverse();

                //Split the id
                splitId( intersectionIds[0]);
            }
            else {
                idText.text = pitId + "000001";
            }

            photoPrefix = idText.text;
        }
    }

    signal addedFeature

    onAddedFeature: {
        console.log("addedFeature")
    }

    signal addInspectionFeaturesChanged
    onAddInspectionFeaturesChanged:  {
        console.log("from the custom signal in the form")
    }

    Flickable {
        id: flickable
        anchors.fill: parent

        onWidthChanged: console.log(width, height)

        contentHeight: cll.height
        flickableDirection: Flickable.VerticalFlick

        interactive: contentHeight > height

        boundsBehavior: Flickable.StopAtBounds

        ColumnLayout{
            id: cll
            clip: true
            spacing: 5
            anchors {
                left: parent.left
                right: parent.right
            }

            Photos_FileFolder{
                id: fileFolder
            }

            Section_Header {
                Layout.fillWidth: true
                headerText: "Intersection Details"
            }

            TextField {
                id: idText

                Layout.fillWidth: true
                font{
                    pointSize: 12
                }
                placeholderText: "BP000001"

                text: appContent.isNewFeature ? "" : appContent.forms_Section.existingInspectionId
                readOnly: true

                onTextChanged: {
                    app.jsonFeature.attributes.IntersectionID = text;
                    app.jsonRelatedInspection.attributes.IntersectionID = text;
                }
            }

            Section_Header{
                Layout.fillWidth: true
                headerText: "Considerations"
            }

            ColumnLayout{
                Layout.fillWidth: true
                Attribute_Selector {
                    model: considerationsArray
                }
            }

            Section_Header{
                Layout.fillWidth: true
                headerText: "Optional Questions"

                Image {
                    anchors {
                        right:parent.right
                        top: parent.top
                        bottom: parent.bottom
                        margins: 2
                    }
                    rotation: optionalQuestions.visible ? 180 : 0
                    source: "../icons/ic_arrow_drop_down_circle_white_3x.png"
                    fillMode: Image.PreserveAspectFit
                }

                MouseArea{
                    anchors.fill: parent
                    onClicked: optionalQuestions.visible = !optionalQuestions.visible
                }
            }

            ColumnLayout{
                id: optionalQuestions
                Layout.fillWidth: true
                visible: false
                Attribute_Selector {
                    Layout.alignment: Qt.AlignTop
                    model: considerationsOptionalArray
                }
            }

            Section_Header {
                Layout.fillWidth: true
                headerText: "Comments"
            }

            TextField {
                id: txtComments
                Layout.fillWidth: true
                font{
                    pointSize: 14
                }

                onTextChanged: {
                    app.jsonRelatedInspection.attributes.Comments = text;
                }
            }


            RowLayout {
                id: furtherActionSection
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter

                Image {
                    source: buttonPressed == 1 ? "../icons/ic_map_ORANGE_64pt.png" : "../icons/ic_map_64pt.png"
                    width: 75
                    height: width
                    fillMode: Image.PreserveAspectFit

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            buttonPressed = 1
                            stackMapCamera.pageToPush = "map"
                        }
                    }
                }

                Image {
                    source: buttonPressed == 2 ? "../icons/ic_note_add_ORANGE_18dp.png" : "../icons/ic_note_add_black_18dp.png"
                    enabled: idText.text > ""
                    width: 75
                    height: width
                    fillMode: Image.PreserveAspectFit
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            buttonPressed = 2
                            stackMapCamera.pageToPush = "actions"
                            console.log("actions clicked focus:", clicked);
                        }
                    }
                    Component.onCompleted: console.log("actions component focus:", focus);
                }

                Image {
                    source: buttonPressed == 3 ? "../icons/ic_photo_camera_Orange_36pt_2x.png" : "../icons/ic_add_a_photo_white_48dp.png"
                    enabled: idText.text > ""
                    width: 75
                    height: width
                    fillMode: Image.PreserveAspectFit
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            buttonPressed = 3;
                            stackMapCamera.isFormCamera = true;
                            stackMapCamera.pageToPush = "photos";
                        }
                    }
                }
            }

            RowLayout {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignBottom

                property Forms_FurtherAction forms_furtherAction

                Button {
                    id: submitButton
                    text: "Submit"

                    enabled: idText.text > ""

                    signal addFileIndex() //See this geonet entry - https://geonet.esri.com/message/569625#comment-569625

                    onAddFileIndex: {
                        if  (gdbComponents.gdbInspectionsFeatureTable.addAttachmentStatus === Enums.AttachmentEditStatusCompleted){
                            addImagesCount++;

                            console.log("from custom signal", gdbComponents.gdbInspectionsFeatureTable.addAttachmentStatus, addImagesCount);

                            var fileCount = fileFolder.fileNames().length;
                            if (addImagesCount === fileCount){
                                removePhotos();
                            }
                        }
                    }

                    property int inspectionID: -1

                    property int addImagesCount: -1
                    onAddImagesCountChanged: {
                        if (addImagesCount > -1){
                            var filesArray = fileFolder.fileNames();
                            var geodatabaseAttachment = filesArray[addImagesCount];
                            if(geodatabaseAttachment){
                                var gdbAttachment = ArcGISRuntime.createObject("GeodatabaseAttachment");
                                var photoString = fileFolder.path + "/" + geodatabaseAttachment;
                                console.log("about to add:", photoString);;

                                gdbAttachment.loadFromFile(photoString, "image/jpeg");

                                gdbComponents.gdbInspectionsFeatureTable.addAttachment(inspectionID, gdbAttachment);
                            }
                        }
                    }

                    onClicked: {
                        var date = new Date();
                        date = date.getTime();

                        //Check to see the type of sign is to placed
                        console.log("requiredFields")
                        appContent.statusText.text = "requiredFields";
                        console.log(JSON.stringify(requiredFields, undefined, 2));

                        for ( var x in requiredFields){
                            console.log(requiredFields[x].name);
                            console.log(jsonRelatedInspection.attributes[requiredFields[x].name])
                            if (jsonRelatedInspection.attributes[requiredFields[x].name] === false){
                                jsonFeature.attributes.Sign_Type = 1; //"Stop"
                                jsonRelatedInspection.attributes.Sign_Type = 1; // "Stop";
                                break;
                            }
                            else{
                                jsonFeature.attributes.Sign_Type = 0; //"GiveWay"
                                jsonRelatedInspection.attributes.Sign_Type = 0; //"GiveWay";
                            }
                        }
                        //appContent.statusText.text = "sign type" + jsonRelatedInspection.attributes.Sign_Type

                        //------------ GDB FEATURE ------------------
                        var feature = ArcGISRuntime.createObject("GeodatabaseFeature");
                        appContent.statusText.text = "is new feature?" + appContent.isNewFeature

                        if (appContent.isNewFeature){
                            jsonFeature.geometry.x = stackMapCamera.map.mCenter.x;
                            jsonFeature.geometry.y = stackMapCamera.map.mCenter.y;
                            console.log(stackMapCamera.map.offlineLayer.spatialReference.wkid)

                            jsonFeature.attributes.created_date = date;
                            jsonFeature.attributes.last_edited_date = date;
                            jsonFeature.attributes.Status = "A";

                            console.log("JSON feature:", JSON.stringify(jsonFeature, undefined, 2));
                            appContent.statusText.text  = JSON.stringify(jsonFeature, undefined, 2);

                            feature.json = jsonFeature;

                            stackMapCamera.map.offlineLayer.featureTable.error.connect(function(error) {
                                console.log("ERROR", error.description);
                            });

                            //Add the new feature to the GDB
                            var id = gdbComponents.gdbFeatureTable.addFeature(feature);

                            //console.log("feature id", id);
                            appContent.statusText.text = "feature id: " + id;
                        }
                        else {
                            id = forms_Section.existnigInspectionObjectId;
                            appContent.statusText.text = "getting: " + id;

                            jsonFeature.attributes.last_edited_date = date;
                            feature.json = jsonFeature;

                            var updateResult = gdbComponents.gdbFeatureTable.updateFeature(id, feature);
                            if (updateResult){
                                appContent.statusText.text = "updated!"
                                console.log(JSON.stringify(gdbComponents.gdbFeatureTable.feature(id).json, undefined, 2));
                            }
                            else
                                appContent.statusText.text = "The intersection feature didn't update. \nPlease fill the form in again"
                        }

                        if (id !== -1){
                            appContent.statusText.text = ("going to the related table...")
                            console.log("what is the id?", id)

                            //------------ RELATED TABLE ------------------
                            //Get the Global ID to link the related table
                            appContent.statusText.text = "globalId: " + gdbComponents.gdbFeatureTable.feature(id).attributes.GlobalID
                            jsonRelatedInspection.attributes.IntersectionGUID = gdbComponents.gdbFeatureTable.feature(id).attributes.GlobalID;
                            jsonRelatedInspection.attributes.created_date = date;
                            jsonRelatedInspection.attributes.last_edited_date = date;

                            console.log(JSON.stringify(jsonRelatedInspection, undefined, 2));

                            console.log("number of features", gdbComponents.gdbFeatureTable.numberOfFeatures);
                            appContent.statusText.text = "num of features: " + gdbComponents.gdbFeatureTable.numberOfFeatures;

                            var inspectionFeature = ArcGISRuntime.createObject("GeodatabaseFeature");
                            inspectionFeature.json = jsonRelatedInspection;
                            inspectionID = gdbComponents.gdbInspectionsFeatureTable.addFeature(inspectionFeature);

                            console.log("inspection id:", inspectionID, gdbComponents.gdbInspectionsFeatureTable.feature(inspectionID).attributes.GlobalID);

                            appContent.statusText.text = "inspection globalid: " + inspectionID;

                            if(inspectionID > -1 && fileFolder.fileNames().length > 0){
                                console.log("lets add some attachments..")
                                addImagesCount = 0;
                            }

                            //------------ FURTHER ACTIOINS TABLE------------------

                            if (inspectionID !== -1){
                                //WIll have to traverse the objects and add the globalId's from both the feature and inspection
                                for ( var fa in stackMapCamera.furtherAction.furtherActionsArray){
                                    stackMapCamera.furtherAction.furtherActionsArray[fa].attributes.InspectionGUID = gdbComponents.gdbInspectionsFeatureTable.feature(inspectionID).attributes.GlobalID;
                                    stackMapCamera.furtherAction.furtherActionsArray[fa].attributes.IntersectionGUID = jsonRelatedInspection.attributes.IntersectionGUID;

                                    var actionFeature = ArcGISRuntime.createObject("GeodatabaseFeature");
                                    actionFeature.json = stackMapCamera.furtherAction.furtherActionsArray[fa];
                                    console.log ('-----------------')
                                    console.log (JSON.stringify(actionFeature.json, undefined, 2));

                                    var actionId = gdbComponents.gdbFurtherActionTable.addFeature(actionFeature);
                                    if (actionId !== -1){
                                        console.log("further Action id:", actionId, gdbComponents.gdbFurtherActionTable.feature(actionId).attributes.GlobalID);
                                    }
                                }
                            }
                        }
                        else {
                            appContent.statusText.text = "Your feature didn't create."
                        }

                        //------------ FORM & DATA CLEANUP ------------------
                        cleanForm();
                    }

                    Component.onCompleted: {
                        //These connections are linked so you can interact with signals from another QML component not in this file
                        gdbComponents.gdbFeatureTablePoly.queryFeaturesStatusChanged.connect(gdbFeatureTablePolyResult);
                        gdbComponents.gdbFeatureTable.queryFeaturesStatusChanged.connect(gdbFeatureResult);
                        gdbComponents.gdbInspectionsFeatureTable.queryFeaturesStatusChanged.connect(gdbFeatureTableInspectionsResult);
                        gdbComponents.gdbInspectionsFeatureTable.addAttachmentStatusChanged.connect(addFileIndex);

                        //Query to find which polyon you are in
                        gdbComponents.query.geometry = appContent.mapCenter;
                        gdbComponents.query.returnGeometry = false;
                        gdbComponents.query.spatialRelationship = Enums.SpatialRelationshipIntersects;
                        gdbComponents.query.outFields = "PitID";

                        gdbComponents.gdbFeatureTablePoly.queryFeatures(gdbComponents.query);

                        removePhotos();
                    }
                }
                Item {
                    Layout.fillWidth: true
                }

                Button {
                    text: "Cancel"
                    onClicked: {
                        removePhotos();
                        cleanForm();
                    }
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: 20
            }

            Section_Header{
                id: prevHeader
                headerText: "Previous Inspections"
                color: app.info.propertyValue("accent-color")
                visible: !isNewFeature
            }

            ColumnLayout {
                id: cl
                Layout.alignment: Qt.AlignTop
                Layout.fillWidth: true

                spacing: 10
                Repeater {
                    id: prevInspections

                    delegate: Row  {
                        spacing: 15
                        width: pItem.width
                        height: childrenRect.height

                        Image{
                            source: modelData[0] === 0 ? "../images/giveway-sign1.jpg" : "../images/stop-sign.jpg"
                            width: app.info.propertyValue("primary-button-size") * 0.75
                            height: width
                            fillMode: Image.PreserveAspectFit
                        }

                        Text{
                            anchors.verticalCenter: parent.verticalCenter
                            text: new Date(modelData[1]).toDateString()
                            color: app.info.propertyValue("secondary-text-color")
                            font {
                                pointSize: 13
                            }
                        }

                        Text{
                            anchors.verticalCenter: parent.verticalCenter
                            text: "<b>Leader:</b> " + modelData[2]
                            color: app.info.propertyValue("secondary-text-color")
                            font {
                                pointSize: 13
                                italic: true
                            }
                        }
                    }
                }
            }
        }

    }


    function findPreviousRecords(){

    }

    function removePhotos(){
        var files = fileFolder.fileNames("*.jpg");
        for ( var f = 0; f < files.length; f++ ) {
            console.log("removing:", files[f]);
            fileFolder.removeFile(files[f]);
        }
    }

    function cleanForm(){
        statusText.text = "Cleaning form.."
        txtComments.text = "";
        jsonFeature = {"attributes":{}, "geometry":{}};
        jsonRelatedInspection = {"attributes":{}};
        jsonInspectionAttachments = ({});

        appContent.customState = "";
        appContent.isNewFeature = true;

        statusText.text = "finished clear form..";

        stackMapCamera.pageToPush = "map";
        stackMapCamera.isFormCamera = false;
        statusText.text = "";

        buttonPressed = 0;
    }
}


