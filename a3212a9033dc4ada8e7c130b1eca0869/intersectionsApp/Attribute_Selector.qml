import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

import ArcGIS.AppFramework.Runtime 1.0
import ArcGIS.AppFramework.Runtime.Controls 1.0

Repeater {
    ColumnLayout {
        Layout.fillWidth: true
        Item {
            Layout.preferredHeight: 3
            Layout.fillWidth: true
        }
        
        RowLayout {
            Layout.fillWidth: true
            
            Text {
                Layout.alignment: Qt.AlignLeft
                Layout.fillWidth: true
                clip: true
                text: modelData.label
                font{
                    pointSize: 12
                }
                wrapMode: Text.WordWrap
                maximumLineCount: 3
            }
            Switch{
                id: customSwitch
                checked: modelData.required
                
                property string fieldName: modelData.name
                
                Component.onCompleted: {
                    jsonRelatedInspection.attributes[fieldName] = customSwitch.checked;
                }
                
                style: SwitchStyle {
                    groove: Rectangle {
                        implicitWidth: 55
                        implicitHeight: 30
                        radius: implicitHeight * 0.5
                        border.color: app.info.propertyValue("divider-color")
                        border.width: 1
                        color: customSwitch.checked ? app.info.propertyValue("default-primary-color") : app.info.propertyValue("text-primary-color")
                        
                    }
                    handle: Rectangle {
                        implicitWidth: 30
                        implicitHeight: 30
                        radius: implicitHeight * 0.5
                        border.color: app.info.propertyValue("divider-color")
                        border.width: 1
                        color: app.info.propertyValue("text-primary-color")
                    }
                }
                
                onCheckedChanged: {
                    jsonRelatedInspection.attributes[fieldName] = customSwitch.checked;
                }
            }
        }
        
        Rectangle{
            Layout.preferredHeight: 1
            Layout.fillWidth: true
            color: app.info.propertyValue("divider-color")
        }
    }
}
