import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import QtPositioning 5.3

import ArcGIS.AppFramework 1.0
import ArcGIS.AppFramework.Controls 1.0
import ArcGIS.AppFramework.Runtime 1.0
import ArcGIS.AppFramework.Runtime.Controls 1.0

//------------------------------------------------------------------------------

RowLayout {

    property string customState: "sign in"
    property bool isNewFeature: true
    property bool cameraCaptureOnly: false
    property Point mapCenter: stackMapCamera.map.mCenter

    property alias stackMapCamera: stackMapCamera
    property alias isSyncImagePlaying: syncImage.playing
    property alias statusText: statusText
    property alias forms_Section: forms_Section

    Content_Base {
        id: forms_Section
        property string existingInspectionId: ""
        property int existnigInspectionObjectId: -1
        property Feature existingFeature

        property real panelInitialDimension: 0
        property real panelMaxDimension: (app.width * 0.5)
        property bool isPanelMinimised: false

        Layout.fillHeight: true
        Layout.preferredWidth: customState === "" ? panelInitialDimension : panelMaxDimension

        Loader {
            id: loader
            anchors.fill: parent
            anchors.margins: 5
            sourceComponent: customState === "sign in" ? frmSignin : customState === "add new" ? frmAddNew : frmBlank
        }

        Timer{
            id: timer
            interval: 5000
            onRunningChanged: console.log ("app content timer running", running)
        }

        Component {
            id: frmBlank
            Content_Base{}
        }

        Component {
            id: frmSignin
            Forms_SignIn {
                onButtonClicked: customState = ""
            }
        }

        Component {
            id: frmAddNew

            Forms_IntersectionDetails {
                id: fid
                anchors.fill: parent
            }
        }
    }

    Rectangle {
        Layout.fillHeight: true
        Layout.fillWidth: true

        ColumnLayout {
            id: mapCl
            anchors.fill: parent

            spacing: 0

            enabled: forms_Section.state != "sign in"

            Content_Base{
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.maximumHeight: app.info.propertyValue("primary-button-size")
                Flickable{
                    anchors.fill: parent
                    flickableDirection:Flickable.HorizontalFlick
                    //enabled: width < contentWidth
                    //contentWidth: rl.width
                    clip: true
                    RowLayout {
                        id: rl
                        anchors{
                            fill: parent
                            margins: app.info.propertyValue("primary-button-size") * 0.1
                        }
                        //Layout.fillHeight: true
                        spacing: app.info.propertyValue("primary-button-size") * 0.3
                        CustomButton {
                            id: addNew
                            imageName: "ic_add_circle_white_48dp.png"
                            enabled: customState == "" && (gdbComponents.gdb.valid || gdbComponents.geodatabaseSyncTask.generateStatus === Enums.GenerateStatusCompleted)

                            MouseArea{
                                anchors.fill: parent
                                onClicked: {
                                    forms_Section.isPanelMinimised = false;
                                    isNewFeature = true;
                                    customState = "add new";
                                    stackMapCamera.pageToPush = "map";
                                    stackMapCamera.isFormCamera = false;
                                }
                            }
                        }

                        CustomButton {
                            id: justPhotos
                            imageName: cameraCaptureOnly ? "ic_map_white_64pt.png" : "ic_photo_camera_white_36pt_2x.png"
                            enabled: customState == "" && (gdbComponents.gdb.valid || gdbComponents.geodatabaseSyncTask.generateStatus === Enums.GenerateStatusCompleted)

                            MouseArea{
                                anchors.fill: parent
                                onClicked: {
                                    cameraCaptureOnly = !cameraCaptureOnly;
                                    if (cameraCaptureOnly){
                                        stackMapCamera.pageToPush = "photos";
                                    }
                                    else {
                                        stackMapCamera.pageToPush = "map";
                                    }
                                }
                            }
                        }

                        CustomButton {
                            id: addInspection
                            imageName: "ic_mode_edit_white_48dp.png"
                            enabled: customState == "" && (gdbComponents.gdb.valid || gdbComponents.geodatabaseSyncTask.generateStatus === Enums.GenerateStatusCompleted)
                            color: enabled ? ma.pressed ? app.info.propertyValue("accent-color") : app.info.propertyValue("default-primary-color") : app.info.propertyValue("accent-color")

                            MouseArea{
                                id: ma
                                anchors.fill: parent
                                onClicked: {
                                    appContent.statusText.text = "Select a sign on the map."
                                    isNewFeature = false;
                                    stackMapCamera.pageToPush = "map";
                                }
                            }
                        }

                        CustomButton {
                            id: help
                            imageName: "ic_help_outline_white_48dp.png"
                            enabled: customState !== "sign in"
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    popInstructions.visible = true;
                                }
                            }
                        }

                        CustomButton {
                            id: syncButton

                            enabled: AppFramework.network.isOnline && customState == "" && serviceInfoTask.featureServiceInfoStatus == Enums.FeatureServiceInfoStatusCompleted

                            AnimatedImage {
                                id: syncImage
                                source: "../icons/loading7_white.gif"
                                anchors.centerIn: parent
                                width: parent.width * 0.75
                                height: width
                                playing: false
                            }

                            MouseArea{
                                enabled: gdbComponents.gdb.valid
                                anchors.fill: parent
                                onClicked: {
                                    syncImage.playing = true
                                    gdbComponents.geodatabaseSyncTask.syncGeodatabase(gdbComponents.gdb.syncGeodatabaseParameters, gdbComponents.gdb);
                                }
                            }
                        }

                        Item {
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                        }
                    }
                }
            }

            Rectangle {
                Layout.fillWidth: true
                Layout.preferredHeight: statusText.text > "" ? statusText.contentHeight * 1.1 : 2
                color: app.info.propertyValue("default-primary-color")
                border.color: "transparent"
                border.width: 0

                //visible: timer.running

                Text{
                    id: statusText
                    anchors {
                        left: parent.left
                        right: parent.right
                        rightMargin: 5
                        top: parent.top
                    }
                    color: app.info.propertyValue("text-primary-color")
                    font{
                        pointSize: 13
                    }
                    wrapMode: Text.WordWrap
                    //onTextChanged: text > "" ? timer.restart() : null
                }

                Image{
                    anchors {
                        right: parent.right
                    }
                    source: "../icons/ic_add_circle_white_48dp.png"
                    rotation: 45
                    height: statusText.paintedHeight
                    width: height
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: statusText.text = "";
                }
            }

            Stack_MapCamera{
                id: stackMapCamera
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }
    }

    Connections{
        target: AppFramework.network
        onIsOnlineChanged: statusText.text = "Disconnected from the network but you can continue to collect data."
    }
}
