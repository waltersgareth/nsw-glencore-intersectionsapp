import QtQuick 2.0

import ArcGIS.AppFramework 1.0
import ArcGIS.AppFramework.Controls 1.0
import ArcGIS.AppFramework.Runtime 1.0
import ArcGIS.AppFramework.Runtime.Controls 1.0

Item {
    property alias gdbFileInfo: gdbFileInfo
    //property alias offlineLayer: offLineLayer
    property alias gdb: gdb
    property alias gdbFeatureTable: gdbFeatureTable
    property alias feature: feature
    property alias inpsections : inpsections

    property alias gdbFeatureTablePoly :gdbFeatureTablePoly
    property alias pits : pits

    property alias gdbInspectionsFeatureTable: gdbInspectionsFeatureTable
    property alias gdbFurtherActionTable: gdbFurtherActionTable
    property alias geodatabaseSyncTask: geodatabaseSyncTask
    property alias generateGeodatabaseParameters: generateGeodatabaseParameters
    property alias query: query
    property alias idQuery: idQuery
    property alias inspectionQuery: inspectionQuery

    FileInfo {
        id: gdbFileInfo
        filePath:  gdbPath + "/" + gdbName
    }

    Query {
        id: query
    }

    Query {
        id: idQuery
    }

    Query {
        id: inspectionQuery
    }

    GenerateGeodatabaseParameters {
        id: generateGeodatabaseParameters
    }

    Geodatabase{
        id: gdb
        path: gdbFileInfo.exists ? gdbPath + "/" + gdbName : geodatabaseSyncTask.geodatabasePath

        onValidChanged: {
            if (valid) {
                console.log ("gdb valid?", valid);
            }
        }
    }

    GeodatabaseFeature {
        id: feature
    }

    GeodatabaseSyncStatusInfo {
        id: syncStatusInfo
    }

    GeodatabaseSyncTask {
        id: geodatabaseSyncTask
        url: app.info.propertyValue("featureServiceUrl")

        onGenerateStatusChanged: {
            if (generateStatus === Enums.GenerateStatusCompleted) {
                appContent.statusText.text = geodatabasePath;
                //appContent.statusText.text = "Select a feature";
                appContent.isSyncImagePlaying = false;


            } else if (generateStatus === Enums.GenerateStatusErrored) {
                appContent.statusText.text = "Error: " + generateGeodatabaseError.message + " Code= "  + generateGeodatabaseError.code.toString() + " "  + generateGeodatabaseError.details;
                appContent.isSyncImagePlaying = false;
            }
            else if ( generateStatus === Enums.GenerateStatusInProgress){
                appContent.isSyncImagePlaying = true
            }
        }

        onGeodatabaseSyncStatusInfoChanged: {
            if (geodatabaseSyncStatusInfo.status === Enums.GeodatabaseStatusUploadingDelta) {
                var deltaProgress = geodatabaseSyncStatusInfo.deltaUploadProgress/1000;
                var deltaSize = geodatabaseSyncStatusInfo.deltaSize/1000;
                appContent.statusText.text = geodatabaseSyncStatusInfo.statusString + " " + String(deltaProgress) + " of " + String(deltaSize) + " KBs..";
            } else
                appContent.statusText.text = geodatabaseSyncStatusInfo.statusString + "..";
            if (geodatabaseSyncStatusInfo.status !== GeodatabaseSyncStatusInfo.Cancelled)
                syncStatusInfo.json = geodatabaseSyncStatusInfo.json;
        }

        onSyncStatusChanged: {
            if (syncStatus === Enums.SyncStatusCompleted) {
                appContent.statusText.text = "Sync completed. You may continue editing the features.";
                appContent.isSyncImagePlaying = false;

            }
            if (syncStatus === Enums.SyncStatusErrored) {
                appContent.statusText.text = "Error: " + syncGeodatabaseError.message + " Code= "  + syncGeodatabaseError.code.toString() + " "  + syncGeodatabaseError.details;
                appContent.isSyncImagePlaying = false;
                console.log("Error: " + syncGeodatabaseError.message + " Code= "  + syncGeodatabaseError.code.toString() + " "  + syncGeodatabaseError.details);
                console.log(JSON.stringify(syncErrors, undefined, 2));
            }
        }
    }

    GeodatabaseFeatureTable {
        id: gdbFeatureTablePoly
        geodatabase: gdb.valid ? gdb : null
        featureServiceLayerId: 1
    }

    GeodatabaseFeatureServiceTable {
        id: pits
        url:  app.info.propertyValue("featureServiceUrl") + "/1"
    }

    GeodatabaseFeatureTable {
        id: gdbFeatureTable
        geodatabase: gdb.valid ? gdb : null
        featureServiceLayerId: 0

        onFeatureTableStatusChanged: {
            console.log("featureTableStatus", featureTableStatus);
            if (featureTableStatus == Enums.FeatureTableStatusInitialized){
                console.log("gdbFeatureTable editable?", isEditable);
            }
        }
    }

    GeodatabaseFeatureServiceTable {
        id: inpsections
        url:  app.info.propertyValue("featureServiceUrl") + "/0"
    }

    GeodatabaseFeatureTable {
        id: gdbInspectionsFeatureTable
        geodatabase: gdb.valid ? gdb : null
        featureServiceLayerId: 3

        onGeodatabaseChanged: gdb.valid ? appContent.statusText.text = "related table is Valid" : appContent.statusText.text = "related table is not valid"

        onAddAttachmentStatusChanged: {
            if (addAttachmentStatus === Enums.AttachmentEditStatusCompleted) {
                console.log("added a photo");
            }
            else if (addAttachmentStatus === Enums.AttachmentEditStatusErrored) {
                //console.log("applyAttachmentEditsErrors: " + applyAttachmentEditsErrors.length);
            }
            else if (addAttachmentStatus === Enums.AttachmentEditStatusInProgress){
                console.log("attachment in progress..");
            }
            else if (addAttachmentStatus === Enums.AttachmentEditStatusReady){
                console.log("attachments are ready");
            }
        }
    }

    GeodatabaseFeatureTable {
        id: gdbFurtherActionTable
        geodatabase: gdb.valid ? gdb : null
        featureServiceLayerId: 2

        onGeodatabaseChanged: gdb.valid ? appContent.statusText.text = "Further Action table is Valid" : appContent.statusText.text = "Further action table is not valid"
    }
}
