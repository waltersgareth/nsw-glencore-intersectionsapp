import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import QtPositioning 5.3

import ArcGIS.AppFramework 1.0
import ArcGIS.AppFramework.Controls 1.0
import ArcGIS.AppFramework.Runtime 1.0
import ArcGIS.AppFramework.Runtime.Controls 1.0

//------------------------------------------------------------------------------

Rectangle {
    height: titleText.paintedHeight + titleText.anchors.margins * 2
    color: app.info.propertyValue("dark-primary-color", "darkblue")
    
    Image{
        source: "../images/safecoal_2.png"
        anchors{
            left: parent.left
            top: parent.top
            bottom: parent.bottom
            margins: 4
        }
        fillMode: Image.PreserveAspectFit
    }
    
    Text {
        id: titleText
        
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            margins: 2 * AppFramework.displayScaleFactor
        }
        
        text: app.info.title
        color: app.info.propertyValue("text-primary-color", "white")
        font {
            pointSize: 22
        }
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        maximumLineCount: 2
        elide: Text.ElideRight
        horizontalAlignment: Text.AlignHCenter
    }
    
    Row {
        spacing: 3
        anchors {
            right: parent.right
            top: parent.top
            bottom: parent.bottom
            margins: 1
        }
        Image {
            height: parent.height
            width: height
            source: "../icons/ic_location_off_white_48dp.png"
        }
        Image {
            height: parent.height
            width: height
            source: "../icons/ic_signal_wifi_statusbar_connected_no_internet_white_26x24dp.png"
        }
    }
}
