# README #

Glencore Mining - Intersection inspections app

### Quick Guide ###

* Add new intersections
* Add inspections to existing intersections

### AppStudio Set up ###
If you want to use the repo folder in AppStudio
* Download to your local repo folder
* Best option is to create a symbolic in your AppStudio Apps folder (C:\Users\<USERNAME>\ArcGIS\AppStudio\Apps) - http://www.howtogeek.com/howto/16226/complete-guide-to-symbolic-links-symlinks-on-windows-or-linux/